async function getPosts(){
    const elPosts = document.getElementById('posts');

    try{
        const response = await Poster.getPosts();

        response.forEach(post => {
            elPosts.appendChild(Poster.createPostTemplate(post));
        });
    }
    catch(e){
        console.error(e);
    }
}

getPosts();

async function searchPosts(){
    const elPosts = document.getElementById('posts');
    const searchPhrase = document.getElementById('search-posts').value;

    elPosts.innerHTML = "";

    try{
        const response = await Poster.getPosts();

        const result = response.filter(resp => resp.title.includes(searchPhrase));

        result.forEach(post => {
            elPosts.appendChild(Poster.createPostTemplate(post));
        });
    }
    catch(e){
        console.error(e);
    }
}