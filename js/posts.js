// IIFE.
const Poster = (function(){

    // Private
    let posts = [];

    // Public
    return{
        getPosts(){
            return fetch('https://jsonplaceholder.typicode.com/posts')
                    .then(resp => resp.json());
        },
        createPostTemplate(post){
            const postDiv = document.createElement('div');
            const postTitle = document.createElement('h3');
            const postBody = document.createElement('p');
            const postLink = document.createElement('a');

            postTitle.innerText = post.title;            
            postBody.innerText = post.body;
            postLink.innerText = 'View comments';
            postLink.href = `/comments.html?post=${post.id}`;

            postDiv.appendChild(postTitle);
            postDiv.appendChild(postBody);
            postDiv.appendChild(postLink);

            return postDiv;
        },
        getComments(){
            return fetch('https://jsonplaceholder.typicode.com/comments')
                    .then(resp => resp.json());
        },
        createCommentTemplate(comment){
            const commentDiv = document.createElement('div');
            const commentTitle = document.createElement('h3');
            const commentEmail = document.createElement('h5');
            const commentBody = document.createElement('p');

            commentTitle.innerText = comment.name;
            commentEmail.innerText = comment.email;
            commentBody.innerText = comment.body;

            commentDiv.appendChild(commentTitle);
            commentDiv.appendChild(commentEmail);
            commentDiv.appendChild(commentBody);

            return commentDiv;
        }
    }
})();