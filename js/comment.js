function getParam(){
    // Gets the url
    let url = window.location.search;
    // Gets the post id
    let params = url.split('=');
    let postId = params[1];

    document.getElementById("commentPageTitle").append(postId);

    return postId;
}


async function getComments(){
    const elComments = document.getElementById("comments");
    
    try{
        const response = await Poster.getComments();

        const postId = getParam();

        const results = response.filter(resp => resp.postId == postId);

        results.forEach(comment => {
            elComments.appendChild(Poster.createCommentTemplate(comment));
        });
    }
    catch(e){
        console.error(e);
    }
}

getComments();