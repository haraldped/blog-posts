# Blog Posts
Added a comments.html page and a comment.js file. When you click "View comments" you will be redirected to the comments page with a post-id inside the url. I use this post-id to search after comments for the current post. 

I also added a search-bar to search for a specific post. In the index.js file there is a "searchPosts" function that handles search functionality.